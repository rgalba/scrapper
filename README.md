# Scrapper

This is a simple application to synchronize and show the 100 Top sites from Alexa Rank.

  - Syncrhonize top ranked sites
  - Browse ranked sites
  - Edit site information

> Scrapper is someone who is very good at fighting and coming out of a fight winning. [Urban Dictionary]

> Harness the power of analytics to grow your business online. Alexa has traffic metrics across the web—an invaluable source for competitive intelligence and strategic insight. [Alexa]

### Version
0.0.5

### Tech
Scrapper uses a number of open source projects to work properly:

* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [Ruby on Rails] - fullstack web-applications framework
* [Mechanize] - it makes automated web interaction easy

And of course Scrapper itself is open source with a [public repository]
 on Bitbucket.

### Installation

You need Ruby (prefer to use RVM):

```sh
$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
$ \curl -sSL https://get.rvm.io | bash -s stable
```

project setup:

```sh
$ git clone [git-repo-url] scrapper
$ cd scrapper
$ rvm install 2.2.0
$ bundle install
$ rake db:setup
$ rake db:migrate
$ rake scrapper:sync
$ rails s
```
### Syncrhonizing records
```sh
$ rake scrapper:sync
```

### Todos

 - Write Tests
 - Rethink Github Save
 - Add Code Comments
 - Add Code climate integration
 - Use Rubocop
 - Link shortener

License
----

MIT


**Free Software, for sure!**

   [alexa]: <http://www.alexa.com>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [public repository]: <http://bitbucket.org/rgalba/scrapper>
   [Urban Dictionary]: <http://pt.urbandictionary.com/define.php?term=scrapper&defid=4073023>
   [Ruby on Rails]: <http://rubyonrails.org>
   [Twitter Bootstrap]: <http://getbootstrap.com>
   [Mechanize]: <https://github.com/sparklemotion/mechanize>
