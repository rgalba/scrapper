class Site < ActiveRecord::Base
  has_attached_file :screenshot, styles: { medium: "1024x768>", thumb: "200x200>" }, default_url: "missing-image.png"
  validates_attachment_content_type :screenshot, content_type: /\Aimage\/.*\Z/
end
