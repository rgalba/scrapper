class SitesController < ApplicationController
  before_action :set_site, only: [:show, :edit, :update, :destroy]

  # GET /sites
  def index
    @sites = Site.page params[:page]
  end

  # GET /sites/1
  def show
  end

  # GET /sites/1/edit
  def edit
    @site = Site.find(params[:id])
  end

  # PATCH/PUT /sites/1
  def update
    if @site.update(site_params)
      respond_to do |format|
        format.json { head :no_content }
        format.js {}
      end
    else
      format.json { render json: @site.errors,
                         status: :unprocessable_entity }
    end
  end

  # DELETE /sites/1
  def destroy
    @site.destroy
    redirect_to sites_url, notice: 'Site was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def site_params
      params.require(:site).permit(:name, :url, :screenshot)
    end
end
