require 'timeout'
require "#{Rails.root}/lib/crawler"

namespace :scrapper do
  desc "Synchronize 100 Alexa Top sites data."
  task sync: :environment do
     puts "[Scrapper]: Synchronizing top sites..."
     Site.destroy_all

     sites = Scrapper::Crawler.top_sites()

     sites.each_with_index do |l, i|
       site = Site.new()
       site.name = l.text
       site.url = l.href
       site.alexa_rank = i + 1
       site.save
     end

     puts "[Scrapper]: Synchronization done."
     puts "[Scrapper]: A total of #{Site.count} sites was synchronized."
  end

  desc "[Scrapper]: Save a screenshot for all sites"
  task screenshots: :environment do
    sites = Site.all.order(alexa_rank: :desc)
    return if sites.empty?

    sites.each do |site|
      begin
        Timeout::timeout(10) do
          site.screenshot = Scrapper::Crawler.take_screenshot(site.url, site.alexa_rank)
        end
        site.save
        puts "#{site.name} screenshot saved"
      rescue Timeout::Error => e
        puts "[Scrapper]: screenshot ignored"
      end
    end
  end
end
