require 'rubygems'
require 'mechanize'
require 'screencap'

module Scrapper
  class Crawler
    URL = 'http://www.alexa.com/topsites/global'

    def self.links(url)
      agent = Mechanize.new { |a| a.user_agent_alias = 'Mac Safari' }
      page = agent.get(url)
      links = page.links
      sites = []
      links.each do |l|
        if l.href =~ /siteinfo/
          l.href.gsub! "\/siteinfo\/", "http:\/\/"
          sites << l
        end
      end
      sites
    end

    def self.top_sites()
      links = self.links(URL)
      links += links("#{URL};1")
      links += links("#{URL};2")
      links += links("#{URL};3")
    end

    def self.take_screenshot(url, index)
      f = Screencap::Fetcher.new(url)
      f.fetch(
        :width => 1024,
        :height => 600
      )
    end
  end
end
